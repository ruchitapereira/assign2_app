class TeamsController < ApplicationController
  before_action :logged_in_user, only: [:create, :index, :edit, :update]
  def show
      @teams = Team.all
  end
  def new
    @team = Team.new
  end
  
   def index
    @teams = Team.paginate(page: params[:page])
   end
   
   def edit
    @team = Team.find(params[:id])
   end
   
  def update
    @team = Team.find(params[:id])
    flash[:success] = "Marks Added"
        if @team.update_attributes(team_params)
           flash[:success] = "Marks Added"
          render 'edit'
        else
          render 'edit'
        end
  end
  
  def create
    @team = Team.new(team_params)
    flash[:success] = @team.TeamName
    # Not the final implementation!
    if @team.save
       redirect_to '/ruchita'
      flash[:success] = "Team Added"
      # Handle a successful save.
    else
      flash[:error] = "Team Not Added"
      redirect_to '/ruchita'
    end
  end
  private
  
  def team_params
      params.require(:team).permit(:TeamName, :Synchronization, :difficulty, :correctness, :Stability, :Costume)
      
  end
 
  
  
end
