class Team < ApplicationRecord
    validates :TeamName, presence: true
    validates :Synchronization, presence: false
    validates :difficulty, presence: false
    validates :correctness, presence: false
    validates :Stability, presence: false
    validates :Costume, presence: false
end
