class CreateTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :teams do |t|
      t.string :TeamName 
      t.text :Synchronization 
      t.text :difficulty 
      t.text :correctness 
      t.text :Stability 
      t.text :Costume 

      t.timestamps
    end
  end
end
