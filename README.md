# README

You can run the code using: https://polar-escarpment-23571.herokuapp.com/

Things you may want to cover:

* Ruby version
`Ruby 2.3.3`

* System dependencies
`Rails 5.0.2`

* Configuration
Git: `git version 2.10.2.windows.1`

* Database creation
`SQLite3`

* Database initialization  
Run `rails db:setup` from root folder   
Run `rails db:migrate` from root folder                          

* How to run the test suite
From the root folder run `rails test`

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions :We have deployed our project on Heroku.We can push the master to heroku using git push heroku master command

* ...